<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('addData', [ApiController::class, 'addData']);
Route::get('getAllData', [ApiController::class, 'getAllData']);
Route::get('getDataById', [ApiController::class, 'getDataById']);
Route::delete('deleteData', [ApiController::class, 'deleteData']);
Route::get('getDataWhere', [ApiController::class, 'getDataWhere']);
Route::put('updateData1', [ApiController::class, 'updateData1']);
Route::post('check_email_id', [ApiController::class, 'check_email_id']);
Route::post('insertDataWithUuid', [ApiController::class, 'insertDataWithUuid']);
Route::post('getAllDataWhere', [ApiController::class, 'getAllDataWhere']);

Route::post('executeSql', [ApiController::class, 'executeSql']);
