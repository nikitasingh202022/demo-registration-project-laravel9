<?php

use App\Http\Controllers\Admin\DashbordController;
use App\Http\Controllers\Admin\nikita\ayushi\DikshaController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\StudentSubject;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/* -----------------Dashbord Student Fee --------------------*/
Route::get('addStudent', [StudentController::class, 'addStudent'])->name('Dashbord.addStudent');
Route::get('addStudent/{id}', [StudentController::class, 'addStudent']);
Route::get('action', [StudentController::class, 'action'])->name('Dashbord.action');
Route::post('insertStudentData', [StudentController::class, 'insertStudentData']);
Route::post('deleteData', [StudentController::class, 'deleteData']);
Route::get('getStudents', [StudentController::class, 'getStudents']);
Route::get('editStudent/{id}', [StudentController::class, 'editStudent']);

/* Details My Contact Person */
Route::get('contactList', [ContactController::class, 'contactList'])->name('Dashbord.contactList');
Route::get('contactList/{id}', [ContactController::class, 'contactList']);
Route::get('actionContact', [ContactController::class, 'actionContact'])->name('Dashbord.actionContact');
Route::post('insertContactDetails', [ContactController::class, 'insertContactDetails']);
Route::post('deleteContact', [ContactController::class, 'deleteContact']);
Route::get('getContactDetails', [ContactController::class, 'getContactDetails']);
Route::get('edit/{contactId}', [ContactController::class, 'edit']);


/* Subjects Of Students */
Route::get('studentSubjectsList', [StudentSubject::class, 'index'])->name('Dashbord.studentSubjectsList');
Route::prefix('user')->middleware(['auth', 'isUsers'])->group(function () {
});

Auth::routes();
Route::prefix('admin')->middleware(['auth', 'isAdmin'])->group(function () {
    Route::get('userList', [UserController::class, 'userList'])->name('auth.userList');
    Route::get('showUserData', [UserController::class, 'showUserData']);
    Route::post('addUser', [UserController::class, 'addUser']);
});


Route::get("dashbord", [DashbordController::class, 'index']);
Route::get('/home', [HomeController::class, 'index'])->name('home');
