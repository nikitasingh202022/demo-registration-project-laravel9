<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_contact', function (Blueprint $table) {
            $table->string('uuid');
            $table->increments('contactId');
            $table->string('contactProfile');
            $table->string('contactPerson');
            $table->string('contactNumber');
            $table->string('contactFather');
            $table->string('contactMother');
            $table->string('contactAddress');
            $table->string('study');
            $table->string('numberOfH');
            $table->string('numberOfI');
            // $table->string('hobbies');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_contact');
    }
};
