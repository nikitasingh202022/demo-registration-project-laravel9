<?php

namespace App\Repositry;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CommonRepositry implements ICommonRepositry
{
    public function insertData($table, $data)
    {
        $query = DB::table($table)->insert($data);
        if ($query) {
            return $query;
        } else {
            return 0;
        }
    }
    public function getAllData($table, $key = "")
    {
        if (isset($key) && !empty($key)) {
            DB::orderBy($key, 'DESC');
        }
        $query = DB::table($table)->get();
        if ($query) {
            return $query;
        } else {
            return 0;
        }
        if ($query->count() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function getAllDataWhere($table, $select, $column, $opt, $id)
    {
        $query = DB::table($table)->select($select)->where($column, $opt, $id)->get();
        return $query;
    }
    public function deleteData($table, $cond)
    {
        DB::enableQueryLog();
        $query = DB::table($table)->where($cond)->delete();
        return  $query;
    }

    public function updateData($table, $cond, $data)
    {
        $result = DB::table($table)->where($cond)->update($data);
        if ($result) {
            return $result;
        } else {
            return 0;
        }
    }
    public function insertDataWithUuid($table, $data)
    {

        $uuid = (string) Str::uuid();

        $data['uuid'] =  $uuid;
        $query = DB::table($table)->insert($data);
        if ($query) {
            return $query;
        } else {
            return 0;
        }
    }

    public function check_email_id($table, $select, $con)
    {
        $query = DB::table($table)->select($select)->where($con)->get();
        if ($query) {
            return $query;
        } else {
            return "Faild";
        }
    }


    public function getDataById($table, $select, $con)
    {
        $query = DB::table($table)->select($select)->where($con)->get();
        if ($query) {
            return $query;
        } else {
            return "Faild";
        }
    }

    public function joins($table, $table1,){

    }

    
    public function executeSql($table, $sql, $type)
    {

        // $query = DB::table($table)->newQuery($sql);
        // switch ($type) {

        //     case 'insert':
        //         $result = $query;
        //         break;

        //     case 'update':
        //         $result = $query;
        //         break;

        //     case 'delete':
        //         $result = $query;
        //         break;
        //     case 'result_array':
        //         $result = $query->result_array();
        //         break;
        //     case 'row_array':
        //         $result = $query->row_array();
        //         break;
        //     case 'num_rows':
        //         $result = $query->num_rows();
        //         break;
        //     default:
        //         $result = 'Failed';
        //         break;
        // }
        // // mysqli_next_result(DB::table($table)->conn_id);
        // return $result;
    }
}
