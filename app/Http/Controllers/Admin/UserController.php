<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositry\ICommonRepositry;
use Illuminate\Http\Request;
use DataTables;

class UserController extends Controller
{
    public $users;
    public function __construct(ICommonRepositry $users)
    {
        $this->users = $users;
    }
    public function userList()
    {
        return view('auth.userList');
    }

    public function showUserData(Request $request)
    {

        if (request()->ajax()) {
            $data = $this->users->getAllData('users');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0);"  data-id="' . $row->id . '"   data-original-title="edit" class="editContact btn btn-primary btn-sm ">Edit</a>       
                           <a href="javascript:void(0)" data-id="' . $row->id . '"      class="delete btn btn-danger edit">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    public function addUser(Request $request)
    {
        $validation = $request->validate([
            'profile' =>  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validation) {
            $data['userId'] = $request->input('userId');
            $data['profile'] = $request->input('profile');
            $data['name'] = $request->input('name');
            $data['email'] = $request->input('email');
            $data['password'] = $request->input('password');
            if ($profile = $request->file('profile')) {
                $destinationPath = 'images/';
                $profileImage = date('YmdHis') . "." . $profile->getClientOriginalExtension();
                $profile->move($destinationPath, $profileImage);
                $data['profile'] = "$profileImage";


                // if($studentImage){
                if (isset($request['userId']) && $request['userId'] != '') {
                    $id = $request['userId'];
                    $updateContact = $this->users->updateData('users', ["userId" => $request->userId], $data);
                    if ($updateContact) {
                        return redirect("/admin/userList");
                    } else {
                        return "Update Faild";
                    }
                } else {
                    $result = $this->users->insertDataWithUuid('users', $data);
                    if ($result) {
                        return redirect('/admin/userList');
                    } else {
                        return "Insert Faild";
                    }
                }
            } else {
                return "Image Not Insert";
            }
        } else {
            return "Faild Validation";
        }
    }
}
