<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositry\ICommonRepositry;
use DataTables;

class ContactController extends Controller
{
    public $contact;
    public function __construct(ICommonRepositry $contact)
    {
        $this->contact = $contact;
    }
    public function contactList()
    {
        return view('Dashbord.contactList');
    }

    public function insertContactDetails(Request $request)
    {
        $validation = $request->validate([
            'contactProfile' =>  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'contactPerson' => 'required',
            'contactNumber' => 'required',
            'contactFather' => 'required',
            'contactMother' => 'required',
            'contactAddress' => 'required',
            'study' => 'required',
            'numberOfH' => 'required',
            'numberOfI' => 'required'
        ]);
        if ($validation) {
            $data['contactId'] = $request->input('contactId');
            $data['contactProfile'] = $request->input('contactProfile');
            $data['contactPerson'] = $request->input('contactPerson');
            $data['contactNumber'] = $request->input('contactNumber');
            $data['contactFather'] = $request->input('contactFather');
            $data['contactMother'] = $request->input('contactMother');
            $data['contactAddress'] = $request->input('contactAddress');
            $data['study'] = $request->input('study');
            $data['numberOfH'] = $request->input('numberOfH');
            $data['numberOfI'] = $request->input('numberOfI');
            if ($contactProfile = $request->file('contactProfile')) {
                $destinationPath = 'images/';
                $profileImage = date('YmdHis') . "." . $contactProfile->getClientOriginalExtension();
                $contactProfile->move($destinationPath, $profileImage);
                $data['contactProfile'] = "$profileImage";


                // if($studentImage){
                if (isset($request['contactId']) && $request['contactId'] != '') {
                    $id = $request['contactId'];
                    $updateContact = $this->contact->updateData('tbl_contact', ["contactId" => $request->contactId], $data);
                    if ($updateContact) {
                        return redirect("/contactList");
                    } else {
                        return "Update Faild";
                    }
                } else {
                    $result = $this->contact->insertDataWithUuid('tbl_contact', $data);
                    if ($result) {
                        return redirect('/contactList');
                    } else {
                        return "Insert Faild";
                    }
                }
            } else {
                return "Image Not Insert";
            }
        } else {
            return "Faild Validation";
        }
    }
    public function getContactDetails(Request $request)
    {

        if (request()->ajax()) {
            $data = $this->contact->getAllData('tbl_contact');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0);"  data-id="' . $row->contactId . '"   data-original-title="edit" class="editContact btn btn-primary btn-sm ">Edit</a>       
                           <a href="javascript:void(0)" data-id="' . $row->contactId . '"      class="delete btn btn-danger edit">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('/');
    }
    public function edit(Request $request)
    {
        $result = $this->contact->getDataById('tbl_contact', "*", ["contactId" => $request->contactId]);
        if ($result) {
            return $result;
        } else {
            return "Faild";
        }
    }
    public function deleteContact(Request $request)
    {

        $data = $this->contact->deleteData('tbl_contact', ["contactId" => $request->contactId]);
        if ($data) {
            return $data;
        } else {
            return "Faild";
        }
    }
}
