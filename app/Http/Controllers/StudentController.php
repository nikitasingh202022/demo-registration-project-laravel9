<?php

namespace App\Http\Controllers;

use App\Models\StudentModel;
use App\Repositry\ICommonRepositry;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DataTables;

class StudentController extends Controller
{
    public $student;

    public function __construct(ICommonRepositry $student)
    {
        $this->student = $student;
    }

    public function addStudent(Request $request)
    {
        return view('Dashbord.addStudent');
    }

    public function action()
    {
        return view('Dashbord.action');
    }
    public function insertStudentData(Request $request)
    {
        $validation = $request->validate([
            'studentName' => 'required',
            'studentFather' => 'required',
            'studentMother' => 'required',
            'studentfee' => 'required',
            'studentAddress' => 'required'
        ]);

        if ($validation) {
            $data['studentName'] = $request->input('studentName');
            $data['studentFather'] = $request->input('studentFather');
            $data['studentMother'] = $request->input('studentMother');
            $data['studentfee'] = $request->input('studentfee');
            $data['studentAddress'] = $request->input('studentAddress');
            if (isset($request['id']) && $request['id'] != '') {
                $id = $request['id'];
                $updateStudentData = $this->student->updateData('student_models', ["id" => $request->id], $data);
                if ($updateStudentData) {
                    return redirect("/addStudent");
                } else {
                    return "Not Update";
                }
            } else {
                $result = $this->student->insertDataWithUuid('student_models', $data);
                if ($result) {
                    return redirect("/addStudent");
                } else {
                    return "Not Insert";
                }
            }
        } else {
            return redirect('/addStudent');
        }
    }

    public function deleteData(Request $request)
    {
        $query = $this->student->deleteData('student_models', ["id" => $request->id]);
        if ($query) {
            return $query;
        } else {
            return "Faild";
        }
    }
    public function editStudent(Request $request)
    {
        $query = $this->student->getDataById('student_models', "*", ["id" => $request->id]);
        if ($query) {
            return $query;
        } else {
            return "Faild";
        }
    }

    public function getStudents(Request $request)
    {

        if (request()->ajax()) {
            $data = $this->student->getAllData('student_models');
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0);"  data-id="' . $row->id . '"   data-original-title="edit" class="editStudentData btn btn-primary btn-sm ">Edit</a>       
                    <a href="javascript:void(0)" data-id="' . $row->id . '"      class="delete btn btn-danger edit">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('/');
    }
}
