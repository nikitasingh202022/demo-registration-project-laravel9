<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (Auth::user()->role_as == '0') { // 1=Admin and 0= Users
            return $next($request);
        } else {
            return redirect('/home')->with('status', 'Access Denied! As You are not an Admin');
        }
        return redirect('/login')->with('status', 'Please Login First');
    }
}
