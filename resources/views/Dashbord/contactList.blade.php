@extends('layouts.app')
@section('content')
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="card-header" id="modelHeading" onclick="refreshPage1()">{{ __('Add Contact Details') }}</div>

                <button type="button" class="btn btn-default" style="margin-left:auto!important;" data-bs-dismiss="modal">X</button>


                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form method="post" id="dtudentForm" action="{{url('/insertContactDetails')}}" enctype="multipart/form-data">
                    @csrf
                    <a href="{{url('/studentList')}}" class="btn-sm btn-warning btn-lg m-t-n-xs">Contact List</a>
                    <div class="row mb-3">
                        <label for="contactProfile" class="col-md-4 col-form-label text-md-end">{{ __('Profile') }}</label>

                        <div class="col-md-6">
                            <input id="contactProfile" type="file" class="form-control @error('contactProfile') is-invalid @enderror" name="contactProfile" value="" required autocomplete="new-contactProfile">
                            <img id="blah" src="" alt="your image" height="50px" width="50px" />
                            @error('contactProfile')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="contactPerson" class="col-md-4 col-form-label text-md-end">{{ __('Contact Person') }}</label>

                        <div class="col-md-6">
                            <input id="contactPerson" type="text" class="form-control @error('contactPerson	') is-invalid @enderror" name="contactPerson" required placeholder="Enter your Contact Person" autocomplete="contactPerson" autofocus>

                            @error('contactPerson ')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="contactNumber" class="col-md-4 col-form-label text-md-end">{{ __('Contact Number') }}</label>

                        <div class="col-md-6">
                            <input id="contactNumber" type="text" class="form-control @error('contactNumber') is-invalid @enderror" name="contactNumber" value="" required placeholder="Enter your Number" autocomplete="new-contactNumber">

                            @error('contactNumber')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="contactFather" class="col-md-4 col-form-label text-md-end">{{ __('Contact Father') }}</label>

                        <div class="col-md-6">
                            <input id="contactFather" type="text" class="form-control @error('contactFather') is-invalid @enderror" name="contactFather" required placeholder="Enter your Father" autocomplete="new-contactFather">

                            @error('contactFather')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="contactMother" class="col-md-4 col-form-label text-md-end">{{ __('Student Mother ') }}</label>

                        <div class="col-md-6">
                            <input id="contactMother" type="text" class="form-control @error('contactMother') is-invalid @enderror" name="contactMother" value="" required placeholder="Enter your Mother" autocomplete="contactMother">

                            @error('contactMother')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="contactAddress" class="col-md-4 col-form-label text-md-end">{{ __('Contact Address') }}</label>

                        <div class="col-md-6">
                            <input id="contactAddress" type="text" class="form-control @error('contactAddress') is-invalid @enderror" name="contactAddress" required placeholder="Enter your Address" autocomplete="new-contactAddress">
                            @error('contactAddress')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">

                        <label for="study" class="col-md-4 col-form-label text-md-end">{{ __('Qualification') }}</label>

                        <div class="col-md-6">
                            <select class="form-control @error('study') is-invalid @enderror" id="study" required placeholder="Enter your Qualification" autocomplete="new-study" name="study">
                                <option value="">--Select Qualification--</option>
                                <option value="UG">UG</option>
                                <option value="PG">PG</option>
                            </select>

                            @error('study')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="numberOfH" class="col-md-4 col-form-label text-md-end">{{ __('Number Of 10') }}</label>

                        <div class="col-md-6">
                            <input id="numberOfH" type="number" class="form-control @error('numberOfH') is-invalid @enderror" name="numberOfH" value="" required placeholder="Enter your Number Of 10" autocomplete="new-numberOfH">

                            @error('numberOfH')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="numberOfI" class="col-md-4 col-form-label text-md-end">{{ __('Number Of 12') }}</label>

                        <div class="col-md-6">
                            <input id="numberOfI" type="number" class="form-control @error('numberOfI') is-invalid @enderror" name="numberOfI" value="" required placeholder="Enter your Number Of 12" autocomplete="new-numberOfI">
                            <input type="hidden" class="contactId" id="contactId" name="contactId">
                            @error('numberOfI')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" id="btn-save" onclick="dtudentForm()" class="btn btn-primary">
                                {{ __('Submit') }}
                            </button>
                            <button type="button" class=" fa-light fa-star" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<center><a onclick="openModel()" id="btn" class="btn btn-sm btn-warning btn-lg m-t-n-xs"><i class="fa fa-plus" title="Add Student">Add Contact Details</i></a></center>
<div class="container mt-5">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <table class="table table-bordered yajra-datatable" id="">
        <thead>
            <tr>
                <th>#</th>
                <th>Profile</th>
                <th>Name</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    function openModel() {
        $('#btn').click(function() {
            $('#ajaxModel').modal('show');
        });
    };

    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('getContactDetails') }}",
            columns: [{
                    data: 'contactId',
                    name: 'contactId'
                },
                {
                    data: 'contactProfile',
                    name: 'contactProfile',
                    render: function(data, type, full, meta) {
                        return "<img src=\"images/" + data + "\" height=\"50,weith=\"20\" alt = 'No Image' />";
                    }
                }, {
                    data: 'contactPerson',
                    name: 'contactPerson'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ]
        });


    });
    $('body').on('click', '.delete', function() {
        if (confirm("Delete Record?") == true) {
            var contactId = $(this).data('id');
            // ajax
            $.ajax({
                type: "POST",
                url: "{{ url('deleteContact') }}",
                data: {
                    contactId: contactId
                },
                dataType: 'json',
                success: function(res) {
                    var oTable = $('.yajra-datatable').dataTable();
                    oTable.fnDraw(false);
                }
            });
        }
    });
    $('body').on('click', '.editContact', function() {
        var contactId = $(this).data('id');

        $.get("{{url('/edit')}}" + '/' + contactId, function(data) {
            var imgFile = "images/" + data[0].contactProfile;
            $('#modelHeading').html("Edit Product");
            $('#btn-save').val("Edit Contact");
            $('#ajaxModel').modal('show');
            $('#contactId').val(data[0].contactId);
            $('#blah').attr('src', imgFile);
            $('#contactProfile').attr('src', imgFile);
            $('#contactPerson').val(data[0].contactPerson);
            $('#contactNumber').val(data[0].contactNumber);
            $('#contactFather').val(data[0].contactFather);
            $('#contactMother').val(data[0].contactMother);
            $('#contactAddress').val(data[0].contactAddress);
            $('#study').val(data[0].study);
            $('#numberOfH').val(data[0].numberOfH);
            $('#numberOfI').val(data[0].numberOfI);
        });
    });

    $('#modelHeading').click(function() { // Reload page on button click.
        location.reload(true);
    });
    contactProfile.onchange = evt => {
        const [file] = contactProfile.files
        if (file) {
            blah.src = URL.createObjectURL(file)
        }
    }
</script>
@endsection