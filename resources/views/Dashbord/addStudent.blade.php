@extends('layouts.app')
@section('content')

<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="card-header" id="modalHeading">{{ __('Add Student') }}</div>

                <button type="button" class="btn btn-default" style="margin-left:auto!important;" data-bs-dismiss="modal">X</button>


                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form method="post" id="dtudentForm" action="{{url('/insertStudentData')}}">
                    @csrf
                    <a href="{{url('/addStudent')}}">Student List</a>
                    <div class="row mb-3">
                        <label for="studentName" class="col-md-4 col-form-label text-md-end">{{ __('Student Name') }}</label>

                        <div class="col-md-6">
                            <input id="studentName" type="text" class="form-control @error('studentName') is-invalid @enderror" name="studentName" value="{{ old('newProductName') }}" required autocomplete="newProductName" autofocus>

                            @error('studentName')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="studentFather" class="col-md-4 col-form-label text-md-end">{{ __('Student Father') }}</label>

                        <div class="col-md-6">
                            <input id="studentFather" type="text" class="form-control @error('studentFather') is-invalid @enderror" name="studentFather" value="" required autocomplete="new-studentFather">

                            @error('studentFather')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="studentMother" class="col-md-4 col-form-label text-md-end">{{ __('Student Mother') }}</label>

                        <div class="col-md-6">
                            <input id="studentMother" type="text" class="form-control @error('studentMother') is-invalid @enderror" name="studentMother" required autocomplete="new-studentMother">

                            @error('studentMother')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="studentfee" class="col-md-4 col-form-label text-md-end">{{ __('Student Fee') }}</label>

                        <div class="col-md-6">
                            <input id="studentfee" type="number" class="form-control @error('studentfee') is-invalid @enderror" name="studentfee" required autocomplete="new-studentfee">

                            @error('studentfee')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="studentAddress" class="col-md-4 col-form-label text-md-end">{{ __('Student Address') }}</label>

                        <div class="col-md-6">
                            <input id="studentAddress" type="text" class="form-control @error('studentAddress') is-invalid @enderror" name="studentAddress" required autocomplete="new-studentAddress">
                            <input type="hidden" name="id" id="id" value="">

                            @error('studentAddress')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" id="btn-save" onclick="dtudentForm()" class="btn btn-primary">
                                {{ __('Submit') }}
                            </button>
                            <button type="button" class=" fa-light fa-star" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<center><a onclick="openModel()" id="btn" class="btn btn-sm btn-primary btn-lg m-t-n-xs"><i class="fa fa-plus" title="Add Student">Add Student</i></a></center>
<div class="container mt-5">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <table class="table table-bordered yajra-datatable" id="">
        <thead>
            <tr>
                <th>#</th>
                <th>Name </th>
                <th>Father Name</th>
                <th>Mother Name</th>
                <th>Fee</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>



<script type="text/javascript">
    function openModel() {
        $('#btn').click(function() {
            $('#ajaxModel').modal('show');
        });
    };


    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('.yajra-datatable').DataTable({
            className: 'dt-control',
            orderable: false,
            data: null,
            defaultContent: '',
            processing: true,
            serverSide: true,
            ajax: "{{ url('/getStudents') }}",
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'studentName',
                    name: 'studentName',
                    render: function(data, type, row, meta) {
                        return data + "  " +
                            '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="' + row +
                            '" data-original-title="Show" id="showSubject" class="show btn inverse ">+</a>' + '<br>' + '<div class = "container" id="showStudentSub" style = "max-width: 100px;"></div>';


                    },
                    sortable: true,
                    searchable: false

                },

                {
                    data: 'studentFather',
                    name: 'studentFather'
                },
                {
                    data: 'studentMother',
                    name: 'studentMother'
                },
                {
                    data: 'studentfee',
                    name: 'studentfee'
                },
                {
                    data: 'studentAddress',
                    name: 'studentAddress'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ]
        });

    });

    $('body').on('click', '#showSubject', function() {
        var id = $(this).attr('id');

        newRowAdd = `<div class = "container" id="` + id + `" style = "max-width: 100px;">
            
            <div>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>c++</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>`;
        $('#showStudentSub').append(newRowAdd);
    });

    // function format(d) {
    //     // `d` is the original data object for the row
    //     return (
    //         '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
    //         '<tr>' +
    //         '<td>Full name:</td>' +
    //         '<td>' +
    //         d.studentName +
    //         '</td>' +
    //         '</tr>' +
    //         '<tr>' +
    //         '<td>Extension number:</td>' +
    //         '<td>' +
    //         d.studentName +
    //         '</td>' +
    //         '</tr>' +
    //         '<tr>' +
    //         '<td>Extra info:</td>' +
    //         '<td>And any further details here (images etc)...</td>' +
    //         '</tr>' +
    //         '</table>'
    //     );
    // }






    $('body').on('click', '.delete', function() {
        if (confirm("Delete Record?") == true) {
            var id = $(this).data('id');
            $.ajax({
                type: "POST",
                url: "{{ url('deleteData') }}",
                data: {
                    id: id
                },
                dataType: 'json',
                success: function(res) {
                    var oTable = $('.yajra-datatable').dataTable();
                    oTable.fnDraw(false);
                }
            });
        }
    });
    $('body').on('click', '.editStudentData', function() {
        var id = $(this).data('id');
        $.get("{{url('/editStudent')}}" + '/' + id, function(data) {
            $('#modalHeading').html("Edit Student Data");
            $('#btn-save').val("Edit Student");
            $('#ajaxModel').modal('show');
            $('#studentName').val(data[0].studentName);
            $('#studentFather').val(data[0].studentFather);
            $('#studentMother').val(data[0].studentMother);
            $('#studentfee').val(data[0].studentfee);
            $('#id').val(data[0].id);
            $('#studentAddress').val(data[0].studentAddress);
        });
    });


    // Add event listener for opening and closing details
    $('#yajra-datatable tbody').on('click', 'td.dt-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });













    // $(document).ready(function() {
    //     var table = $('.yajra-datatable').DataTable({
    //         ajax: "{{ url('/getStudents') }}",
    //         columns: [{
    //                 className: 'dt-control',
    //                 orderable: false,
    //                 data: null,
    //                 defaultContent: '',
    //             },
    //             {
    //                 data: 'name'
    //             },
    //             {
    //                 data: 'position'
    //             },
    //             {
    //                 data: 'office'
    //             },
    //             {
    //                 data: 'salary'
    //             },
    //         ],
    //         order: [
    //             [1, 'asc']
    //         ],
    //     });


    // $('.yajra-datatable tbody').on('click', 'td.dt-control', function() {
    //     alert('ghfh');
    //     var tr = $(this).closest('tr');
    //     var row = table.row(tr);

    //     if (row.child.isShown()) {

    //         row.child.hide();
    //         tr.removeClass('shown');
    //     } else {

    //         row.child(format(row.data())).show();
    //         tr.addClass('shown');
    //     }
    // });
</script>
@endsection