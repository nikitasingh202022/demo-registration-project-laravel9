@extends('layouts.app')
@section('content')

<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="card-header" id="modalHeading">{{ __('Add User ') }}</div>

                <button type="button" class="btn btn-default" style="margin-left:auto!important;" data-bs-dismiss="modal">X</button>


                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form method="post" id="dtudentForm" action="{{url('/admin/addUser')}}">
                    @csrf
                    <a href="{{url('/addStudent')}}">Student List</a>
                    <div class="row mb-3">
                        <label for="profile" class="col-md-4 col-form-label text-md-end">{{ __('Profile') }}</label>

                        <div class="col-md-6">
                            <input id="profile" type="file" class="form-control @error('profile') is-invalid @enderror" name="profile" value="" required autocomplete="new-profile">
                            <img id="blah" src="" alt="your image" height="50px" width="50px" />
                            @error('profile')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">

                        <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('User Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('newProductName') }}" required autocomplete="newProductName" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('User Email') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="" required autocomplete="new-email">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('User  Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" id="btn-save" onclick="dtudentForm()" class="btn btn-primary">
                                {{ __('Submit') }}
                            </button>
                            <button type="button" class=" fa-light fa-star" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<center><a onclick="openModel()" id="btn" class="btn btn-sm btn-primary btn-lg m-t-n-xs"><i class="fa fa-plus" title="Add User">Add User</i></a></center>
<div class="container mt-5">
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <table class="table table-bordered yajra-datatable" id="">
        <thead>
            <tr>
                <th>#</th>
                <th>Profile</th>
                <th>Name</th>
                <th>User Email</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    function openModel() {
        $('#btn').click(function() {
            $('#ajaxModel').modal('show');
        });
    };
    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('/admin/showUserData') }}",
            columns: [{
                    data: 'userId',
                    name: 'userId'
                },
                {
                    data: 'profile',
                    name: 'profile'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ]
        });

    });
    // $('body').on('click', '.delete', function() {
    //     if (confirm("Delete Record?") == true) {
    //         var id = $(this).data('id');
    //         $.ajax({
    //             type: "POST",
    //             url: "{{ url('deleteData') }}",
    //             data: {
    //                 userId: userId
    //             },
    //             dataType: 'json',
    //             success: function(res) {
    //                 var oTable = $('.yajra-datatable').dataTable();
    //                 oTable.fnDraw(false);
    //             }
    //         });
    //     }
    // });
    // $('body').on('click', '.editStudentData', function() {
    //     var id = $(this).data('id');
    //     $.get("{{url('/showUserData')}}" + '/' + id, function(data) {
    //         $('#modalHeading').html("Edit User");
    //         $('#btn-save').val("Edit User");
    //         $('#ajaxModel').modal('show');
    //         $('#blah').attr('src', imgFile);
    //         $('#profile').attr('src', imgFile);
    //         $('#name').val(data[0].name);
    //         $('#email').val(data[0].email);
    //         $('#id').val(data[0].id);
    //     });
    // });

    contactProfile.onchange = evt => {
        const [file] = contactProfile.files
        if (file) {
            blah.src = URL.createObjectURL(file)
        }
    }
</script>
@endsection